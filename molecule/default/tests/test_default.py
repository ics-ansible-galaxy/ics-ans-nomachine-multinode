import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('nomachine')


def test_nxserver(host):
    service = host.service("nxserver")
    assert service.is_running
    assert service.is_enabled
